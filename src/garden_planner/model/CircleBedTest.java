package garden_planner.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CircleBedTest {

    @Test
    public void getWidth() {
        CircleBed circleBed = new CircleBed(1);
        Assert.assertEquals(1, circleBed.getWidth(), 0.001);
    }

    @Test
    public void getHeight() {
        CircleBed circleBed = new CircleBed(1);
        Assert.assertEquals(1, circleBed.getHeight(), 0.001);
    }

    @Test
    public void getArea() {
        CircleBed circleBed = new CircleBed(1);
        Assert.assertEquals(0.7853, circleBed.getArea(), 0.001);
    }

    @Test
    public void getPerimeter() {
        CircleBed circleBed = new CircleBed(1);
        Assert.assertEquals(3.1415, circleBed.getPerimeter(), 0.001);
    }

}