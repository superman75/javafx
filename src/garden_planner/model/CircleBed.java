package garden_planner.model;

public class CircleBed extends GardenBed {
    private double diameter = 1.0;


    public CircleBed(double diameter) {
        this.diameter = diameter;
    }

    public void setDiameter(double diameter){
        this.diameter = diameter;
    }

    @Override
    public double getWidth() {
        return diameter;
    }

    @Override
    public double getHeight() {
        return diameter;
    }

    @Override
    public double getArea() {
        return Math.PI * diameter * diameter / 4;
    }

    @Override
    public double getPerimeter() {
        return Math.PI * diameter;
    }

    @Override
    public String toString() {
        return String.format("Circle %.2f %.2f %.2f", left, top, diameter);
    }
}
