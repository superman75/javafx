package garden_planner.gui;

import garden_planner.model.CircleBed;
import garden_planner.model.GardenBed;
import garden_planner.model.GardenPlanner;
import garden_planner.model.RectBed;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Controller {
    private GardenPlanner planner;
    private boolean moveFlag = false;
    private boolean resizeFlag = false;

    @FXML
    TextField widthField;
    @FXML
    TextField heightField;
    @FXML
    TextField leftField;
    @FXML
    TextField topField;
    @FXML
    TextField lengthField;
    @FXML
    TextField areaField;
    @FXML
    TextField costField;

    @FXML
    Pane garden;

    @FXML
    public void initialize() throws FileNotFoundException {

        planner = new GardenPlanner();
//        this.planner.createBasicDesign();
        this.planner.readBeds(new Scanner(new File("fancy_garden.txt")));
        this.planner.recalculateTotals();
        updateGUI();
    }

    @FXML
    protected void changeTop(KeyEvent event){
        double top = Double.parseDouble(topField.getText());
        planner.getBeds().get(0).setTop(top);
        updateGUI();
    }

    @FXML
    public void createRect(MouseEvent mouseEvent) {
        System.out.println("+Rect is clicked");
        planner.getBeds().add(new RectBed());
        updateGUI();
    }

    @FXML
    public void createCircle(MouseEvent mouseEvent) {
        System.out.println("+Circle is clicked");
        planner.getBeds().add(new CircleBed(1));
        updateGUI();
    }

    public Controller() {
    }


    private void updateGUI(){
        garden.getChildren().clear();
        garden.setStyle("-fx-background-image: url('https://i.pinimg.com/originals/07/6e/13/076e13045ae44e4429035f162cad2a34.jpg')");
        for (GardenBed bed: planner.getBeds()) {
            if(bed.toString().startsWith("Circle")){
                Circle circle = new Circle();
                double radius = 50*bed.getWidth()/2;
                circle.setRadius(radius);
                circle.setCenterX(50*bed.getLeft() + radius);
                circle.setCenterY(50*bed.getTop() + radius);
                circle.setFill(new ImagePattern(new Image("file:flowers.png"),0,0,1,1,true));

                circle.setOnMouseDragged(event -> {

                    if(moveFlag){
                        circle.setCenterX(event.getX());
                        circle.setCenterY(event.getY());
                        bed.setLeft((event.getX()-radius)/50);
                        bed.setTop((event.getY()-radius)/50);

                    }
                    if (resizeFlag){
                        circle.setRadius(Math.pow(Math.pow(event.getX()-circle.getCenterX(),2) + Math.pow(event.getY()-circle.getCenterY(),2),0.5));
                        ((CircleBed) bed).setDiameter((Math.pow(Math.pow(event.getX()-circle.getCenterX(),2) + Math.pow(event.getY()-circle.getCenterY(),2),0.5))/25);
                    }

                });

                circle.setOnMousePressed(event -> {
                    if (Math.abs(event.getX()-circle.getCenterX())<10 && Math.abs(event.getY()-circle.getCenterY())<10){
                        moveFlag = true;
                    }
                    if (Math.abs(Math.pow(Math.pow(event.getX()-circle.getCenterX(),2) + Math.pow(event.getY()-circle.getCenterY(),2),0.5)-circle.getRadius())<10){
                        resizeFlag = true;
                    }
                });
                circle.setOnMouseReleased(event -> {
                    moveFlag = false;
                    resizeFlag = false;
                });

                garden.getChildren().add(circle);
            } else {
                javafx.scene.shape.Rectangle rect = new javafx.scene.shape.Rectangle();
                rect.setHeight(50*bed.getHeight());
                rect.setWidth(50*bed.getWidth());
                rect.setX(50*bed.getLeft());
                rect.setY(50*bed.getTop());
                rect.setFill(new ImagePattern(new Image("file:vegetables.png"),0,0,1,1,true));

                garden.getChildren().add(rect);

                rect.setOnMouseDragged(event -> {
                    if(moveFlag){
                        rect.setX(event.getX());
                        rect.setY(event.getY());
                        bed.setLeft(event.getX()/50);
                        bed.setTop(event.getY()/50);
                        updateTextField(rect);
                    }
                    if (resizeFlag){
                        rect.setWidth(event.getX()-rect.getX());
                        rect.setHeight(event.getY() -rect.getY());
                        ((RectBed) bed).setWidth((event.getX()-rect.getX())/50);
                        ((RectBed) bed).setHeight((event.getY() -rect.getY())/50);
                        updateTextField(rect);
                    }

                });

                rect.setOnMousePressed(event -> {
                    if (Math.abs(event.getX()-rect.getX())<10 && Math.abs(event.getY()-rect.getY())<10){
                        moveFlag = true;
                    }
                    if (Math.abs(event.getX()-rect.getX()-rect.getWidth())<10 && Math.abs(event.getY()-rect.getY()-rect.getHeight())<10){
                        resizeFlag = true;
                    }
                });
                rect.setOnMouseReleased(event -> {
                    moveFlag = false;
                    resizeFlag = false;
                });

            }

        }


        javafx.scene.shape.Rectangle rect = (javafx.scene.shape.Rectangle) garden.getChildren().get(0);
        updateTextField(rect);
        lengthField.setText(String.valueOf(Math.floor(planner.getTotalWallLength()*100)/100));
        areaField.setText(String.valueOf(Math.floor(planner.getTotalGardenArea()*100)/100));
        costField.setText(String.valueOf(Math.floor(planner.getTotalCost()*100)/100));
    }
    private void updateTextField(Rectangle rect) {
        widthField.setText(String.valueOf(rect.getWidth()));
        heightField.setText(String.valueOf(rect.getHeight()));
        leftField.setText(String.valueOf(rect.getX()));
        topField.setText(String.valueOf(rect.getY()));
    }


}
