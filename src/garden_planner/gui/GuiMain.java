package garden_planner.gui;

import garden_planner.model.GardenBed;
import garden_planner.model.GardenPlanner;
import garden_planner.model.RectBed;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;

import java.awt.*;

public class GuiMain extends Application {

    private GardenPlanner planner;
    private TextField widthField;
    private TextField heightField;
    private TextField leftField;
    private TextField topField;
    private Label widthLabel;
    private Label heightLabel;
    private Label leftLabel;
    private Label topLabel;

    public GuiMain() {
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        // Parent root = FXMLLoader.load(getClass().getResource("garden_gui.fxml"));

        Parent root = FXMLLoader.load(getClass().getResource("garden_gui.fxml"));


        primaryStage.setTitle("Garden Planner");
        primaryStage.setScene(new Scene(root, 600, 400));

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
